﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusicSchoolManagement
{
    public partial class Courses : Form
    {
        public Courses()
        {
            InitializeComponent();
            GetTeachers();
            DisplayCourse();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=DESKTOP-2DFQKOU;Initial Catalog=MusicSchoolDB;Integrated Security=True");

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Homes Obj = new Homes();
            Obj.Show();
            this.Hide();
        }

        private void GetTeachers()
        {
            Con.Open();
            SqlCommand cmd = new SqlCommand("select TNum from TeachersTbl", Con);
            SqlDataReader Rdr;
            Rdr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("TNum", typeof(int));
            dt.Load(Rdr);
            TCb.ValueMember = "TNum";
            TCb.DataSource = dt;
            Con.Close();

        }

        private void FetchTName()
        {
            Con.Open();
            string Query = "Select * from TeachersTbl where TNum='" + TCb.SelectedValue.ToString() + "'";
            SqlCommand cmd = new SqlCommand(Query, Con);
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            foreach(DataRow dr in dt.Rows)
            {
                TNameTb.Text = dr["TName"].ToString();

            }
            Con.Close();
        }

        private void DisplayCourse()
        {
            Con.Open();
            string Query = "Select * from CourseTbl";
            SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
            SqlCommandBuilder cmd = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            CoursesDGV.DataSource = ds.Tables[0];
            Con.Close();
        }
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if(CourseNameTb.Text == "" || TCb.SelectedIndex == -1 || TNameTb.Text == "" || PriceTb.Text == "" || DurationTb.Text == "")
            {
                MessageBox.Show("Missing Infomation");
            }else
            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("insert into CourseTbl(CName, CTId, CTName, Cprice, CDuration) values(@CN, @CTI, @CTN, @CP, @CD)", Con);
                    cmd.Parameters.AddWithValue("@CN", CourseNameTb.Text);
                    cmd.Parameters.AddWithValue("@CTI", TCb.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@CTName", TNameTb.Text);
                    cmd.Parameters.AddWithValue("@Cprice", PriceTb.Text);
                    cmd.Parameters.AddWithValue("@CD", DurationTb.Text);
                    
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Course Added");
                    Con.Close();
                    DisplayCourse();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message);
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Homes Obj = new Homes();
            Obj.Show();
            this.Hide();
        }

        private void TCb_SelectionChangeCommitted(object sender, EventArgs e)
        {
            FetchTName();
        }

        private void CoursesDGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
